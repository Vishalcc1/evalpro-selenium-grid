package Pages;

import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class courseSidebarPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#notifications div a.dropdown-toggle")
    @CacheLookup
    private WebElement _0;

    @FindBy(id = "abt-us")
    @CacheLookup
    private WebElement aboutUs;

    @FindBy(id = "bodhiTreeTitle")
    @CacheLookup
    private WebElement bodhitree;

    @FindBy(id="sideList-assignments")
    @CacheLookup
    private WebElement programLabsStudent;
    
    @FindBy(id="sideList-programming")
    @CacheLookup
    private WebElement programLabsInstructor;
    
    public courseSidebarPage() {
    }

    public courseSidebarPage(WebDriver driver) {
        this();
        this.driver = driver;
        PageFactory.initElements( driver, this);
    }

    public courseSidebarPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
        PageFactory.initElements( driver, this);
    }

    public courseSidebarPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
        PageFactory.initElements( driver, this);
    }
    
    public courseSidebarPage ProgramLabsStudent()
    {
    	programLabsStudent.click();
    	return this;
    }
    
    public courseSidebarPage programLabsInstructor()
    {
    	programLabsInstructor.click();
    	return this;
    }
    
    public void waitForPageLoadInstructor(Duration timeout) {

    	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

    	public Boolean apply(WebDriver driver) {
    		 
    		
    	    return programLabsInstructor.getText().contains("Programming Labs");

    	};
    	};
    	
    	WebDriverWait wait = new WebDriverWait(this.driver, timeout);

    	wait.until(pageLoadCondition);

    	
    }
    
    public void waitForPageLoadStudent(Duration timeout) {

    	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

    	public Boolean apply(WebDriver driver) {
    		 
    		
    	    return programLabsStudent.getText().contains("Programming Labs");

    	};
    	};
    	
    	WebDriverWait wait = new WebDriverWait(this.driver, timeout);

    	wait.until(pageLoadCondition);

    	
    }
   
}