package Pages;

import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import bsh.util.Util;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class assignmentPageInstructor {
	private Map<String, String> data;
	private WebDriver driver;
	private int timeout = 15;
	private int year, month, day, hour, timeslice, minute;

	public assignmentPageInstructor() {
	}

	public assignmentPageInstructor(WebDriver driver) {
		this();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public assignmentPageInstructor(WebDriver driver, Map<String, String> data) {
		this(driver);
		this.data = data;
		PageFactory.initElements(driver, this);
	}

	public assignmentPageInstructor(WebDriver driver, Map<String, String> data, int timeout) {
		this(driver, data);
		this.timeout = timeout;
		PageFactory.initElements(driver, this);
	}

	/*
	 * AddSection ***************************************************************
	 */

	public WebElement getAddSectionElement() {
		return driver.findElement(By.cssSelector("span[class*='glyphicon-plus']"));
	}

	public String getAddSection() {
		return getAddSectionElement().getText();
	}

	public void clickAddSection() {
		getAddSectionElement().click();
	}

	/*
	 * EditAssignment
	 * ***************************************************************
	 */

	public WebElement getEditAssignmentElement() {
		return driver.findElement(By.cssSelector("span[class*='glyphicon-edit']"));
	}

	public String getEditAssignment() {
		return getEditAssignmentElement().getText();
	}

	public void clickEditAssignment() {
		getEditAssignmentElement().click();
	}

	/*
	 * BasicDetails ***************************************************************
	 */

	public WebElement getBasicDetailsElement() {
		return driver.findElement(By.linkText("Basic Details"));
	}

	public void clickBasicDetails() {
		getBasicDetailsElement().click();
	}

	public String getBasicDetails() {
		return getBasicDetailsElement().getText();
	}

	/*
	 * TaAllocation ***************************************************************
	 */

	public WebElement getTaAllocationElement() {
		return driver.findElement(By.linkText("TA Allocation"));
	}

	public void clickTaAllocation() {
		getTaAllocationElement().click();
	}

	/*
	 * ProgrammingDetails
	 * ***************************************************************
	 */

	public WebElement getProgrammingDetailsElement() {
		return driver.findElement(By.linkText("Programming Details"));
	}

	public void clickProgrammingDetails() {
		getProgrammingDetailsElement().click();
	}

	/*
	 * ExamAndOtherDetails
	 * ***************************************************************
	 */

	public WebElement getExamAndOtherDetailsElement() {
		return driver.findElement(By.linkText("Exam And Other Details"));
	}

	public void clickExamAndOtherDetails() {
		getExamAndOtherDetailsElement().click();
	}

	/*
	 * AssignmentName
	 * ***************************************************************
	 */

	public WebElement getAssignmentNameElement() {
		return driver.findElement(By.id("id_name"));
	}

	public String getAssignmentName() {
		return getAssignmentNameElement().getAttribute("value");
	}

	public void setAssignmentName(String value) {
		getAssignmentNameElement().sendKeys(value);
	}

	/*
	 * Document ***************************************************************
	 */

	public WebElement getDocumentElement() {
		return driver.findElement(By.xpath("//div[@id='createAssignment']/div/div[3]/div[2]/div"));
	}

	public String getDocument() {
		return getDocumentElement().getText();
	}

	public void clickDocument() {
		getDocumentElement().click();
	}

	/*
	 * UploadDocuments
	 * ***************************************************************
	 */

	public WebElement getUploadDocumentsElement() {
		return driver.findElement(By.id("id_document"));
	}

	public String getUploadDocuments() {
		return getUploadDocumentsElement().getAttribute("value");
	}

	public void setUploadDocuments(String value) {
		getUploadDocumentsElement().sendKeys(value);
	}

	/*
	 * AllAssignments
	 * ***************************************************************
	 */

	public WebElement getAllAssignmentsElement() {
		return driver.findElement(By.cssSelector("div[id='contenthere'] > div"));
	}

	public void clickAllAssignments() {
		getAllAssignmentsElement().click();
	}
	/*
	 * Date ***************************************************************
	 */

	public WebElement getDateElement(int calendernumber, int row, int col) {
		return driver.findElement(By.cssSelector("html[class*='fontawesome-i2svg-active'] > body > div:nth-of-type("
				+ calendernumber + ") > div:nth-of-type(1) > div:nth-of-type(2) > table > tbody > tr:nth-of-type(" + row
				+ ") > td:nth-of-type(" + col + ") > div"));
	}

	public String getDate(int calendernumber, int row, int col) {
		return getDateElement(calendernumber, row, col).getText();
	}

	public void clickDate(int calendernumber, int row, int col) {
		getDateElement(calendernumber, row, col).click();
	}

	/*
	 * Next Month ***************************************************************
	 */

	public WebElement getMonthElement(int calendernumber) {
		return driver.findElement(By.cssSelector("html[class*='fontawesome-i2svg-active'] > body > div:nth-of-type("
				+ calendernumber + ") > div:nth-of-type(1) > div:nth-of-type(1) > button:nth-of-type(3)"));
	}

	public void clickNxtMonth(int calendernumber, Duration duration) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				try {
					getMonthElement(calendernumber).click();
					return true;
				} catch (Exception e) {
					return false;
				}

			};
		};
		WebDriverWait wait = new WebDriverWait(this.driver, duration);
		wait.until(pageLoadCondition);
	}
	/*
	 * get current month
	 */

	public int getCurrentMonth() {
		int currentmonth;

		GregorianCalendar date = new GregorianCalendar();
		currentmonth = date.get(Calendar.MONTH) + 1;
		return currentmonth;
	}

	public int getCurrentYear() {
		int currentyear;

		GregorianCalendar date = new GregorianCalendar();
		currentyear = date.get(Calendar.YEAR);
		return currentyear;
	}

	public int getCurrentDate() {
		int currentyear;

		GregorianCalendar date = new GregorianCalendar();
		currentyear = date.get(Calendar.DATE);
		return currentyear;
	}

	public int getCurrentHour() {
		int currentyear;

		GregorianCalendar date = new GregorianCalendar();
		currentyear = date.toZonedDateTime() // Convert from legacy class GregorianCalendar to modern ZonedDateTime.
				.getHour();
		return currentyear;
	}

	public int getCurrentMinute() {
		int currentyear;

		GregorianCalendar date = new GregorianCalendar();
		currentyear = date.get(Calendar.MINUTE);
		return currentyear;
	}

	/*
	 * Set Month
	 */
	public boolean setMonth(int calendernumber, int needMonth, int needyear, Duration duration) {
		int currentmonth = getCurrentMonth(), currentyear = getCurrentYear(), getincrement = 0;

		if (currentmonth <= needMonth) {
			getincrement = needMonth - currentmonth + 12 * (needyear - currentyear);

		} else {
			getincrement = (needMonth - currentmonth + 12) % 12 + 12 * (needyear - currentyear - 1);
		}

		if (getincrement < 0) {
			System.out.println("Month Not Set...");
			return false;
		}

		for (int i = 0; i < getincrement; i++) {
			clickNxtMonth(calendernumber, duration);
		}
		System.out.println("Month Set...");
		return true;
	}

	/*
	 * Set Date
	 */
	public boolean setDate(int calendernumber, int day, Duration duration) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {

				try {

					boolean startFromOne = false;
					for (int i = 1; i <= 5; i++) {
						for (int j = 1; j <= 7; j++) {
							if (getDate(calendernumber, i, j).equals("1")) {
								startFromOne = true;
							}

							if (startFromOne && getDate(calendernumber, i, j).equals(String.valueOf(day))) {
								clickDate(calendernumber, i, j);
								System.out.println("Date Set...");
								return true;
							}
						}
					}
					return false;

				} catch (Exception e) {
					return false;
				}

			}
		};

		WebDriverWait wait = new WebDriverWait(this.driver, duration);
		System.out.println("Start setting Date...");
		wait.until(pageLoadCondition);
		return true;

	}

	/*
	 * DownHour ***************************************************************
	 */

	public WebElement getDownHourElement(int calendernumber) {
		return driver.findElement(By.cssSelector("html[class*='fontawesome-i2svg-active'] > body > div:nth-of-type("
				+ calendernumber + ") > div:nth-of-type(2) > button:nth-of-type(2)"));
	}

	public void clickDownHour(int calendernumber) {
		getDownHourElement(calendernumber).click();
	}

	/*
	 * UpHour ***************************************************************
	 */

	public WebElement getUpHourElement(int calendernumber) {
		return driver.findElement(By.cssSelector("html[class*='fontawesome-i2svg-active'] > body > div:nth-of-type("
				+ calendernumber + ") > div:nth-of-type(2) > button:nth-of-type(1)"));
	}

	public void clickUpHour(int calendernumber) {
		getUpHourElement(calendernumber).click();
	}

	/*
	 * add hour ***************************************************************
	 */

	public WebElement getaddhourElement(int calendernumber, int hour) {
		return driver.findElement(
				By.cssSelector("html[class*='fontawesome-i2svg-active'] > body > div:nth-of-type(" + calendernumber
						+ ") > div:nth-of-type(2) > div > div:nth-of-type(1) > div:nth-of-type(" + hour + ")"));
	}

	public boolean setHour(int calendernumber, int hour, Duration duration) throws InterruptedException {

		if (hour < 1 || hour > 24) {
			System.out.println("Hour Not Set...");
			return false;
		}

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				int down = 50;
				while (down > 0) {
					try {
						getaddhourElement(calendernumber, hour).click();
						System.out.println("Hour Set...");
						return true;
					} catch (Exception e) {
						down--;
						if (down > 25) {
							clickDownHour(calendernumber);
						} else {
							clickUpHour(calendernumber);
						}
					}
				}
				return false;
			};
		};

		WebDriverWait wait = new WebDriverWait(this.driver, duration);
		System.out.println("Start setting Hour...");
		wait.until(pageLoadCondition);
		return true;

	}

	/*
	 * Set minute
	 */
	public boolean setMinute(WebElement element, int minute) {
		if (minute < 0 || minute > 59) {
			System.out.print("Minute Not Set...");
			return false;
		}
		element.sendKeys(Keys.BACK_SPACE);
		element.sendKeys(Keys.BACK_SPACE);
		String min=Integer.toString(minute);
		if(min.length()==1)
			min="0"+min;
		element.sendKeys(min);
		System.out.print("Minute Set...");
		return true;
	}

	/*
	 * time shift
	 */
	public int monthtodays() {
		Calendar calendar = new GregorianCalendar();
		return calendar.getMaximum(java.util.Calendar.DAY_OF_MONTH);
	}

	public void addtimeslice(int year, int month, int day, int hour, int minute, int timeslicehour,
			int timesliceminute) {
		minute += timesliceminute;
		if (minute > 59) {
			timeslicehour++;
			minute = minute % 60;
		}
		this.minute = minute;
		this.hour = (hour + timeslicehour) % 24;
		this.day = day;
		this.month = month;
		this.year = year;
		if (this.hour < hour) {
			this.day++;
		}
		if (monthtodays() < this.day) {
			this.day = 1;
			this.month = month + 1;
		}
		if (this.month > 12) {
			this.month = 1;
			this.year = year + 1;
		}

	}

	/*
	 * AvailableFrom ***************************************************************
	 */

	public WebElement getAvailableFromElement() {
		return driver.findElement(By.id("id_publish_on"));
	}

	public String getAvailableFrom() {
		return getAvailableFromElement().getAttribute("value");
	}

	public void setAvailableFrom(Duration duration, int year, int month, int day) throws InterruptedException {

		int calendernumber = 4;
		getAvailableFromElement().click();
		if (!setMonth(calendernumber, month, year, duration)) {
			System.out.println("Date is wrong...");
			return;
		}

		setDate(calendernumber, day,duration);

	}

	/*
	 * Deadline ***************************************************************
	 */

	public WebElement getDeadlineElement() {
		return driver.findElement(By.id("id_deadline"));
	}

	public String getDeadline() {

		return getDeadlineElement().getAttribute("value");
	}

	public void clickDeadline() {
		getDeadlineElement().click();
	}

	public void setDeadline(Duration duration, int year, int month, int day, int hour, int minute, int timeslicehour,
			int timesliceminute) throws InterruptedException {
		int calendernumber = 3;
		clickDeadline();

		System.out.println("Before increment" + minute + " " + hour + " " + day + " " + month + " " + year);

		addtimeslice(year, month, day, hour, minute, timeslicehour, timesliceminute);
		year = this.year;
		month = this.month;
		day = this.day;
		hour = this.hour;
		minute = this.minute;

		System.out.println("After increment " + minute + " " + hour + " " + day + " " + month + " " + year);

		setMonth(calendernumber, month, year, duration);

//		TimeUnit.SECONDS.sleep(1);

		setDate(calendernumber, day,duration);

		setHour(calendernumber, hour + 1, duration);

		setMinute(getDeadlineElement(), minute);

		TimeUnit.SECONDS.sleep(4);

	}

	/*
	 * FreezingDeadline
	 * ***************************************************************
	 */

	public WebElement getFreezingDeadlineElement() {
		return driver.findElement(By.id("id_freezing_deadline"));
	}

	public String getFreezingDeadline() {
		return getFreezingDeadlineElement().getAttribute("value");
	}

	public void setFreezingDeadline(String value) {
		getFreezingDeadlineElement().sendKeys(value);
	}

	/*
	 * SaveContinue ***************************************************************
	 */

	public WebElement getSaveContinueElement() {
		return driver.findElement(By.id("btn_save"));
	}

	public void clickSaveContinue() {
		getSaveContinueElement().click();
	}

	/*
	 * SaveClose ***************************************************************
	 */

	public WebElement getSaveCloseElement() {
		return driver.findElement(By.id("id_submit"));
	}

	public void clickSaveClose() {
		getSaveCloseElement().click();
	}

	/*
	 * ExamDuration ***************************************************************
	 */

	public WebElement getExamDurationElement() {
		return driver.findElement(By.id("id_timeduration"));
	}

	public String getExamDuration() {
		return getExamDurationElement().getAttribute("value");
	}

	public void setExamDuration(String value) {
//			 new WebDriverWait(driver, Duration.ofSeconds(10)).
//			 until(ExpectedCondition.elementToBeClickable(getExamDurationElement())).click();
//		     
		try {
			// Block of code to try
			getExamDurationElement().sendKeys(value);
		} catch (Exception e) {
			// Block of code to handle errors
			System.out.println("OK");
		}

	}

	public void setExamDuration(Duration timeout, String value) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				try {
					getExamDurationElement().clear();
					getExamDurationElement().sendKeys(value);
					return true;
				} catch (Exception e) {
					return false;
				}

			};
		};

		WebDriverWait wait = new WebDriverWait(this.driver, timeout);
		wait.until(pageLoadCondition);
	}

	/*
	 * ExamLateDuration
	 * ***************************************************************
	 */

	public WebElement getExamLateDurationElement() {
		return driver.findElement(By.id("id_late_duration"));
	}

	public String getExamLateDuration() {
		return getExamLateDurationElement().getAttribute("value");
	}

	public void setExamLateDuration(String value) {
		getExamLateDurationElement().sendKeys(value);
	}

	public void setExamLateDuration(Duration timeout, String value) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				try {
					getExamLateDurationElement().clear();
					getExamLateDurationElement().sendKeys(value);
					return true;
				} catch (Exception e) {
					return false;
				}

			};
		};

		WebDriverWait wait = new WebDriverWait(this.driver, timeout);
		wait.until(pageLoadCondition);
	}

	/*
	 * ExamGroupId ***************************************************************
	 */

	public WebElement getExamGroupIdElement() {
		return driver.findElement(By.id("id_exam_group_id"));
	}

	public String getExamGroupId() {
		return getExamGroupIdElement().getAttribute("value");
	}

	public void setExamGroupId(String value) {
		getExamGroupIdElement().sendKeys(value);
	}

	public void setExamGroupId(Duration timeout, String value) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				try {
					getExamGroupIdElement().clear();
					getExamGroupIdElement().sendKeys(value);
					return true;
				} catch (Exception e) {
					return false;
				}

			};
		};

		WebDriverWait wait = new WebDriverWait(this.driver, timeout);
		wait.until(pageLoadCondition);
	}

	/*
	 * IPaddress ***************************************************************
	 */

	public WebElement getIPaddressElement() {
		return driver.findElement(By.id("id_ipaddress"));
	}

	public String getIPaddress() {
		return getIPaddressElement().getAttribute("value");
	}

	public void setIPaddress(String value) {
		getIPaddressElement().sendKeys(value);
	}

	public void setIPaddress(Duration timeout, String value) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				try {
					getIPaddressElement().clear();
					getIPaddressElement().sendKeys(value);
					return true;
				} catch (Exception e) {
					return false;
				}

			};
		};

		WebDriverWait wait = new WebDriverWait(this.driver, timeout);
		wait.until(pageLoadCondition);
	}

	/*
	 * Page load verification
	 */
	public void waitForPageLoad(Duration timeout) {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				System.out.print("okayy...");
				return true;

			};
		};

		WebDriverWait wait = new WebDriverWait(this.driver, timeout);

		wait.until(pageLoadCondition);

	}

}
