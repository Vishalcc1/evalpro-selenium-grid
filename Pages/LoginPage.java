package Pages;


import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(id = "abt-us")
    @CacheLookup
    private WebElement aboutUs;

    @FindBy(id = "bodhiTreeTitle")
    @CacheLookup
    private WebElement bodhitree;

    @FindBy(id = "firstName")
    @CacheLookup
    private WebElement byClickingSignUpYouAgree1;

    @FindBy(id = "lastName")
    @CacheLookup
    private WebElement byClickingSignUpYouAgree2;

    @FindBy(id = "emailAddress")
    @CacheLookup
    private WebElement byClickingSignUpYouAgree3;

    @FindBy(id = "password")
    @CacheLookup
    private WebElement byClickingSignUpYouAgree4;

    @FindBy(id = "confirmPassword")
    @CacheLookup
    private WebElement byClickingSignUpYouAgree5;

    @FindBy(css = "a[href='/accounts/forgotpassword/']")
    @CacheLookup
    private WebElement changeforgotPassword;

    @FindBy(id = "con-us")
    @CacheLookup
    private WebElement contactUs;

    @FindBy(id = "exploreButtonTrigger")
    @CacheLookup
    private WebElement explore;

    @FindBy(css = "a[href=' https://docs.google.com/document/d/1x3Yo3QTw9UTBS03v3loix-x7VPI-XU4V8uFVETy_JQE/edit?usp=sharing']")
    @CacheLookup
    private WebElement haveTroubleLoggingInSeeLogin;

    private final String pageLoadedText = "Sign in to your BodhiTree Account";

    private final String pageUrl = "/accounts/login/";

    @FindBy(css = "a[href='/privacy/']")
    @CacheLookup
    private WebElement privacyPolicy;

    @FindBy(id = "signInTabButton")
    @CacheLookup
    private WebElement signIn1;

    @FindBy(id = "signinSubmit")
    @CacheLookup
    private WebElement signIn2;

    @FindBy(id = "signinUsername")
    @CacheLookup
    private WebElement signInToYourBodhitreeAccountUserName;

    @FindBy(id = "signinPassword")
    @CacheLookup
    private WebElement signInToYourBodhitreeAccountPassword;

    @FindBy(id = "wmd-input")
    @CacheLookup
    private WebElement signInToYourBodhitreeAccount3;

    @FindBy(id = "ddd")
    @CacheLookup
    private WebElement signInWithIitbSso;

    @FindBy(id = "signUpTabButton")
    @CacheLookup
    private WebElement signUp1;

    @FindBy(id = "signupSubmit")
    @CacheLookup
    private WebElement signUp2;

    @FindBy(css = "a[href='/terms/']")
    @CacheLookup
    private WebElement termsOfUse;

    @FindBy(css = "button.navbar-toggle")
    @CacheLookup
    private WebElement toggleNavigation;

    public LoginPage() {
    }

    public LoginPage(WebDriver driver) {
        this();
        this.driver = driver;
        PageFactory.initElements( driver, this);
    }

    public LoginPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;

        PageFactory.initElements( driver, this);
    }

    public LoginPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
        PageFactory.initElements( driver, this);
    }


    /**
     * Click on Sign In Button.
     *
     * @return the LoginPage class instance.
     */
    public LoginPage clickSignInButton() {
    	signIn2.click();
        return this;
    }


    /**
     * Set value to By Clicking Sign Up You Agree To Our Terms Of Use And Our Privacy Policy Text field.
     *
     * @return the LoginPage class instance.
     */
    public LoginPage setByClickingSignUpTextField(String byClickingSignUpYouAgree1Value) {
    	signInToYourBodhitreeAccountUserName.sendKeys(byClickingSignUpYouAgree1Value);
        return this;
    }


    public LoginPage setByClickingSignUpPasswordField(String signInToYourBodhitreeAccount1Value) {
    	signInToYourBodhitreeAccountPassword.sendKeys(signInToYourBodhitreeAccount1Value);
        return this;
    }


    /**
     * Submit the form to target page.
     *
     * @return the LoginPage class instance.
     */
    public LoginPage signIn() {
        clickSignInButton();
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the LoginPage class instance.
     */
    public void waitForPageLoad(Duration timeout) {

    	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

    	public Boolean apply(WebDriver driver) {

    	return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");}

    	};

    	WebDriverWait wait = new WebDriverWait(this.driver, timeout);

    	wait.until(pageLoadCondition);

    	}

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the LoginPage class instance.
     */
    public LoginPage verifyPageUrl() {
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        (wait).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
