package Pages;


import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a.navbar-brand.bd-navbar-brand")
    @CacheLookup
    private WebElement bodhitree;

    @FindBy(id = "exploreButtonTrigger")
    @CacheLookup
    private WebElement explore;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(2) div.dropdown-menu a:nth-of-type(2)")
    @CacheLookup
    private WebElement instructor;

    @FindBy(id = "bodhiMyCourse")
    @CacheLookup
    private WebElement myDashboard;

    
    

    @FindBy(css = "#modal80 div.modal-dialog div.modal-content div:nth-of-type(3) button:nth-of-type(1)")
    @CacheLookup
    private WebElement no2;

    @FindBy(css = "#modal81 div.modal-dialog div.modal-content div:nth-of-type(3) button:nth-of-type(1)")
    @CacheLookup
    private WebElement no3;

    @FindBy(css = "#modal82 div.modal-dialog div.modal-content div:nth-of-type(3) button:nth-of-type(1)")
    @CacheLookup
    private WebElement no4;

    @FindBy(css = "#modal84 div.modal-dialog div.modal-content div:nth-of-type(3) button:nth-of-type(1)")
    @CacheLookup
    private WebElement no5;

    @FindBy(css = "#modal83 div.modal-dialog div.modal-content div:nth-of-type(3) button:nth-of-type(1)")
    @CacheLookup
    private WebElement no6;

    private final String pageLoadedText = "Click on 'Explore' option to register in courses";

    private final String pageUrl = "/frontend/courseware/courseslist";

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(3) div.dropdown-menu a:nth-of-type(1)")
    @CacheLookup
    private WebElement profile;

    @FindBy(id = "modeDropDown")
    @CacheLookup
    private WebElement roleStudent;

    @FindBy(css = "a[href='/accounts/logout/']")
    @CacheLookup
    private WebElement signOut;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(2) div.dropdown-menu a:nth-of-type(1)")
    @CacheLookup
    private WebElement student;

    @FindBy(id = "navbarDropdown")
    @CacheLookup
    private WebElement testStudent;
//    this.data.get("title")
//    @FindBy(css="[title^="+"Test"+"]")
    private WebElement no1;

    public LandingPage() {
    }

    public LandingPage(WebDriver driver) {
        this();
        this.driver = driver;
        
        PageFactory.initElements( driver, this);
    }

    public LandingPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
        PageFactory.initElements( driver, this);
    }

    public LandingPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
        PageFactory.initElements( driver, this);
    }

    public LandingPage openCourse()
    {
//    	WebElement no1=driver.findElement(By.cssSelector("[title^='TEST']"));
    	no1.click();
    	return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the LandingPage class instance.
     */
    public void waitForPageLoad(Duration timeout) {

    	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

    	public Boolean apply(WebDriver driver) {
    		no1 = driver.findElement(By.cssSelector("[title^="+data.get("courseTitle")+"]"));
//    		 WebElement element = driver.findElement(By.cssSelector("[title^='TEST']"));
    	        //Check that the element contains given text?
    	     return no1.getText().contains(data.get("courseCodeTitleText"));

    		};
    	};
    	
    	WebDriverWait wait = new WebDriverWait(this.driver, timeout);

    	wait.until(pageLoadCondition);

    	
    }
    
    
}