package Pages;

import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.sql.Time;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import bsh.util.Util;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class assignmentPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='/courseware/course/88/information']")
    @CacheLookup
    private WebElement aboutCourse;

    @FindBy(css = "a[href='/courseware/course/88/assignments/']")
    @CacheLookup
    private WebElement allAssignments;

    @FindBy(css = "a.navbar-brand")
    @CacheLookup
    private WebElement bodhitree;

    @FindBy(css = "nav:nth-of-type(2) div.container div.collapse.navbar-collapse.navbar-ex1-collapse ul:nth-of-type(1) li a")
    @CacheLookup
    private WebElement bodhitreeAllRightsReserved2023;

    @FindBy(css = "a[href='/contact/']")
    @CacheLookup
    private WebElement contactUs;

    @FindBy(css = "a[href='/courseware/course/88/announcements']")
    @CacheLookup
    private WebElement courseAnnouncements;

    @FindBy(css = "a[href='/courseware/']")
    @CacheLookup
    private WebElement courses;

    @FindBy(css = "a[href='/courseware/course/88/discussion-forum']")
    @CacheLookup
    private WebElement discussionForum;

    @FindBy(css = "a[href='/download/assignment/downloadtestcases/1673']")
    @CacheLookup
    private WebElement download;

    @FindBy(css = "a[href='/assignments/editor/1673']")
    @CacheLookup
    private WebElement editor;

    @FindBy(css = "a[href='/courseware/course/88/email']")
    @CacheLookup
    private WebElement emailNotices;

    @FindBy(css = "#contenthere div:nth-of-type(1) div:nth-of-type(1) ul.breadcrumb div:nth-of-type(1) li a.dropbtn")
    @CacheLookup
    private WebElement exammode;

    @FindBy(css = "a[href='/courseware/course/88/marks']")
    @CacheLookup
    private WebElement grades;

    @FindBy(css = "a[href='/courseware/courseslist/']")
    @CacheLookup
    private WebElement home;

    @FindBy(css = "a[href='/assignments/cribs/1673']")
    @CacheLookup
    private WebElement issues;

    @FindBy(css = "a[href='/courseware/course/88/progress']")
    @CacheLookup
    private WebElement leaderboard;

    @FindBy(css = "a[href='/accounts/logout/']")
    @CacheLookup
    private WebElement logout;

    @FindBy(css = "a[href='/mission/']")
    @CacheLookup
    private WebElement mission;

    @FindBy(css = "a[href='/courseware/course/88']")
    @CacheLookup
    private WebElement multimediaBook;

    private final String pageLoadedText = "No files submitted for this assignment";

    private final String pageUrl = "/assignments/details/1673";

    @FindBy(css = "a[href='/upload/mysubmissions/1673']")
    @CacheLookup
    private WebElement previousVersions;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(4) ul.dropdown-menu li:nth-of-type(1) a")
    @CacheLookup
    private WebElement profile;

    @FindBy(css = "a[href='/courseware/course/88/assignments']")
    @CacheLookup
    private WebElement programmingLabs;

    @FindBy(css = "a[href='/courseware/course/88/schedule']")
    @CacheLookup
    private WebElement schedule;

    @FindBy(id = "id_docfile")
    @CacheLookup
    private WebElement selectOneOrMoreFiles;

    @FindBy(id = "wmd-input")
    @CacheLookup
    private WebElement selectOneOrMoreFiles2;

    @FindBy(css = "a[href='/courseware/course/88/report-grading']")
    @CacheLookup
    private WebElement subjectiveSubmissions;

    @FindBy(css = "a[href='/team/']")
    @CacheLookup
    private WebElement team;

    @FindBy(css = "a.dropdown-toggle")
    @CacheLookup
    private WebElement testStudent;

    @FindBy(id = "assignment-2827")
    @CacheLookup
    private WebElement thisTest1;

    @FindBy(id = "assignment-1668")
    @CacheLookup
    private WebElement tocprogramming;

    @FindBy(css = "button.navbar-toggle")
    @CacheLookup
    private WebElement toggleNavigation;

    private WebElement upload;

    @FindBy(css = "a.btn.btn-primary")
    @CacheLookup
    private WebElement view;

    public assignmentPage() {
    }

    public assignmentPage(WebDriver driver) {
        this();
        this.driver = driver;
        PageFactory.initElements( driver, this);
    }

    public assignmentPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
        PageFactory.initElements( driver, this);
    }

    public assignmentPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
        PageFactory.initElements( driver, this);
    }
    
    public assignmentPage clickEditorLink() {
        editor.click();
        return this;
    }
    
    public assignmentPage uploadFile(String codePath) throws InterruptedException
    {
    	File file = new File(System.getProperty("user.dir") + "/src/Pages/"+"main.c");
    	if(codePath.isEmpty())
    		codePath = file.getAbsolutePath();
    	
    	selectOneOrMoreFiles.sendKeys(codePath);
    	upload.click();
    	
    	 Alert alert = driver.switchTo().alert();		
 		
         // Capturing alert message.    
         String alertMessage= driver.switchTo().alert().getText();		
         		
         // Displaying alert message		
         System.out.println(alertMessage);	
         TimeUnit.SECONDS.sleep(1);
         Assert.assertEquals(alertMessage, "Please refresh editor after uploading the files.");
         		
         // Accepting alert		
         alert.accept();		
    	return this;
    }
    
    public void waitForPageLoad(Duration timeout) {

    	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

    	public Boolean apply(WebDriver driver) {
    		 upload = driver.findElement(By.cssSelector("[value^="+"Upload"+"]"));
    	     return editor.getText().contains("Editor");

    		};
    	};
    	
    	WebDriverWait wait = new WebDriverWait(this.driver, timeout);

    	wait.until(pageLoadCondition);

    	
    }
    

}


