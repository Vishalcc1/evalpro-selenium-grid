package Pages;

import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class assignmentListPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#notifications div a.dropdown-toggle")
    @CacheLookup
    private WebElement _0;


    @FindBy(css = "#assignment_publish_type div:nth-of-type(2) select.form-control")
    @CacheLookup
    private WebElement assignmentDurationhhmmssfreezingExtraTimehhmmss;

    @FindBy(css = "#ex-gr_0 div div:nth-of-type(2) div:nth-of-type(4) a.action_link")
    @CacheLookup
    private WebElement availableFrom;

    @FindBy(css = "#ex-gr_0 div div:nth-of-type(2) div:nth-of-type(5) a.action_link")
    @CacheLookup
    private WebElement availableTill;

    @FindBy(id = "bodhiTreeTitle")
    @CacheLookup
    private WebElement bodhitree;

    @FindBy(css = "button.btn.btn-default")
    @CacheLookup
    private WebElement close2;

    @FindBy(id = "con-us")
    @CacheLookup
    private WebElement contactUs;

    @FindBy(css = "a[href='#collapseOneExamForm']")
    @CacheLookup
    private WebElement createQuiz;

    @FindBy(css = "a[href='/report_grading_app/download_submissions/88']")
    @CacheLookup
    private WebElement downloadAllSubmissions;

    @FindBy(css = "a[href='/report_grading_app/download_xls/88']")
    @CacheLookup
    private WebElement downloadMarksheet;

    @FindBy(css = "#ex-gr_2 div.panel-body.ex-group-inner div:nth-of-type(2) div.row.panel-heading div:nth-of-type(3) a")
    @CacheLookup
    private WebElement endtime;

    @FindBy(css = "#adv_correctness div:nth-of-type(2) select.form-control")
    @CacheLookup
    private WebElement enterValueOfErrorRate1;

    @FindBy(css = "#adv_correctness div:nth-of-type(4) input.form-control[type='number']")
    @CacheLookup
    private WebElement enterValueOfErrorRate2;

    @FindBy(css = "#assignment-exam div:nth-of-type(2) input.form-control[type='text']")
    @CacheLookup
    private WebElement examGroupIdlatedurationhhmmssipRangeEg1;

    @FindBy(css = "#assignment-exam div:nth-of-type(4) input.form-control[type='text']")
    @CacheLookup
    private WebElement examGroupIdlatedurationhhmmssipRangeEg2;

    @FindBy(css = "#assignment-exam div:nth-of-type(6) input.form-control[type='text']")
    @CacheLookup
    private WebElement examGroupIdlatedurationhhmmssipRangeEg3;

    @FindBy(css = "#assignment-exam div:nth-of-type(8) input.form-control[type='text']")
    @CacheLookup
    private WebElement examGroupIdlatedurationhhmmssipRangeEg4;

    @FindBy(css = "#collapseOneExamForm div.panel-body form fieldset div:nth-of-type(2) input.form-control[type='text']")
    @CacheLookup
    private WebElement examNametitle;

    @FindBy(css = "a[href='/assignments/details/1673']")
    @CacheLookup
    private WebElement exammode;

    @FindBy(id = "exploreButtonTrigger")
    @CacheLookup
    private WebElement explore;

    @FindBy(css = "#assignment_duration div:nth-of-type(2) input.form-control[type='text']")
    @CacheLookup
    private WebElement freezingExtraTimehhmmss1;

    @FindBy(css = "#assignment_duration div:nth-of-type(4) input.form-control[type='text']")
    @CacheLookup
    private WebElement freezingExtraTimehhmmss2;

    @FindBy(css = "#assignment_1668 div.row div:nth-of-type(6) button.label.label-primary")
    @CacheLookup
    private WebElement hide;


    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(1) ul.dropdown-menu li:nth-of-type(2) a")
    @CacheLookup
    private WebElement instructor;

    @FindBy(css = "#collapseOneExamForm div.panel-body form fieldset div:nth-of-type(6) input.form-control[type='text']")
    @CacheLookup
    private WebElement ipRange;

    @FindBy(css = "a[title='Assignments Issues']")
    @CacheLookup
    private WebElement issues0;

    @FindBy(css = "#marksModal div.modal-dialog div.modal-content div:nth-of-type(2) form div:nth-of-type(3) input[type='file']")
    @CacheLookup
    private WebElement jsonConfigFile;

    @FindBy(css = "#report-grading-lab div:nth-of-type(1) input.form-control[type='text']")
    @CacheLookup
    private WebElement labNumber;

    @FindBy(css = "a[href='/accounts/logout/']")
    @CacheLookup
    private WebElement logout;

    @FindBy(id = "bodhiMyCourse")
    @CacheLookup
    private WebElement myDashboard;

    @FindBy(css = "#ex-gr_0 div div:nth-of-type(2) div:nth-of-type(1) a.action_link")
    @CacheLookup
    private WebElement name1;

    @FindBy(css = "#ex-gr_2 div.panel-body.ex-group-inner div:nth-of-type(2) div.row.panel-heading div:nth-of-type(1) a")
    @CacheLookup
    private WebElement name2;

    @FindBy(css = "#marksModal div.modal-dialog div.modal-content div:nth-of-type(2) form div:nth-of-type(1) input.form-control")
    @CacheLookup
    private WebElement nameOfGoogleSpreadsheet;

    @FindBy(css = "a.action_link.btn.btn-primary.addAssignment-button")
    @CacheLookup
    private WebElement new1;

    @FindBy(css = "a.btn.btn-primary.addLabReport-button")
    @CacheLookup
    private WebElement new2;

    @FindBy(css = "#ex-gr_0 div div:nth-of-type(2) div:nth-of-type(3) a.action_link")
    @CacheLookup
    private WebElement outOfMarks;

    private final String pageLoadedText = "";

    private final String pageUrl = "/courseware/course/88/exams/";

    @FindBy(css = "a[href='/privacy/']")
    @CacheLookup
    private WebElement privacyPolicy;

    @FindBy(css = "a[href='/assignments/exam/proctoring/88']")
    @CacheLookup
    private WebElement proctor;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(3) ul.dropdown-menu li:nth-of-type(1) a")
    @CacheLookup
    private WebElement profile;

    @FindBy(id = "publish_on")
    @CacheLookup
    private WebElement publishOn;

    @FindBy(css = "a[href='/assignments/repository/88']")
    @CacheLookup
    private WebElement repository500;

    @FindBy(css = "a[title='Change the role of the user.']")
    @CacheLookup
    private WebElement roleInstructor;

    @FindBy(id = "soft_deadline")
    @CacheLookup
    private WebElement softDeadline;

    @FindBy(css = "#ex-gr_2 div.panel-body.ex-group-inner div:nth-of-type(2) div.row.panel-heading div:nth-of-type(2) a")
    @CacheLookup
    private WebElement starttime;

    @FindBy(css = "#ex-gr_0 div div:nth-of-type(2) div:nth-of-type(6) a.action_link")
    @CacheLookup
    private WebElement status;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(1) ul.dropdown-menu li:nth-of-type(1) a")
    @CacheLookup
    private WebElement student;

    @FindBy(css = "#ex-gr_0 div div:nth-of-type(2) div:nth-of-type(2) a.action_link")
    @CacheLookup
    private WebElement submission;

    @FindBy(css = "#collapseOneExamForm div.panel-body form fieldset div:nth-of-type(9) button.btn.btn-primary")
    @CacheLookup
    private WebElement submit;

    @FindBy(css = "#report-grading-lab div:nth-of-type(2) input.form-control[type='text']")
    @CacheLookup
    private WebElement teamSize;

    @FindBy(css = "a[href='/terms/']")
    @CacheLookup
    private WebElement termsOfUse;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(3) a.dropdown-toggle")
    @CacheLookup
    private WebElement testInstructor;

    @FindBy(css = "a[href='/assignments/details/1668']")
    @CacheLookup
    private WebElement tocprogramming;

    @FindBy(css = "button.navbar-toggle")
    @CacheLookup
    private WebElement toggleNavigation;

    @FindBy(css = "#assignment_1673 div.row div:nth-of-type(6) button.label.label-primary")
    @CacheLookup
    private WebElement unhide;

    @FindBy(css = "#marksModal div.modal-dialog div.modal-content div:nth-of-type(2) form div:nth-of-type(2) input.form-control")
    @CacheLookup
    private WebElement urlOfSpreadsheet;

    @FindBy(css = "a.btn.btn-primary.pull-left")
    @CacheLookup
    private WebElement userManual;

    @FindBy(css = "#1 div.panel-body.ex-group-inner div.container div:nth-of-type(1) div:nth-of-type(2) a.btn.btn-primary")
    @CacheLookup
    private WebElement viewMarks;
    
//    @FindBy(linkText="ExamMode")
//    @CacheLookup
    private WebElement assignmentTitle;
    
    public assignmentListPage() {
    }

    public assignmentListPage(WebDriver driver) {
        this();
        this.driver = driver;
        PageFactory.initElements( driver, this);
    }

    public assignmentListPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
        PageFactory.initElements( driver, this);
    }

    public assignmentListPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
        PageFactory.initElements( driver, this);
    }
    
    public assignmentListPage clickProgramAssignment() {

    	assignmentTitle.click();
    	return this;
    }  
    
    public void waitForPageLoad(Duration timeout) {

    	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

    	public Boolean apply(WebDriver driver) {
    		
    		assignmentTitle=driver.findElement(By.linkText(data.get("assignmentTitle")));
    		return assignmentTitle.getText().contains(data.get("assignmentTitle"));

    		};
    	};
    	
    	WebDriverWait wait = new WebDriverWait(this.driver, timeout);

    	wait.until(pageLoadCondition);

    	
    }
  
}