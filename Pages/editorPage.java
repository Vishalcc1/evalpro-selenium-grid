package Pages;

import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.assertEquals;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class editorPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='/courseware/course/88/assignments/']")
    @CacheLookup
    private WebElement allAssignments;

    @FindBy(css = "a.navbar-brand")
    @CacheLookup
    private WebElement bodhitree;

    @FindBy(css = "#shade div.container div.collapse.navbar-collapse.navbar-ex1-collapse ul:nth-of-type(1) li a")
    @CacheLookup
    private WebElement bodhitreeAllRightsReserved2023;

    @FindBy(css = "#myModal div.modal-dialog div.modal-content div:nth-of-type(3) button.btn.btn-default")
    @CacheLookup
    private WebElement close;

    @FindBy(css = "a[href='/contact/']")
    @CacheLookup
    private WebElement contactUs;

    @FindBy(css = "a[href='/courseware/']")
    @CacheLookup
    private WebElement courses;

    @FindBy(id = "display")
    @CacheLookup
    private WebElement display;

    @FindBy(css = "#contenthere div:nth-of-type(4) ul.breadcrumb li:nth-of-type(2) a")
    @CacheLookup
    private WebElement exammode;

    @FindBy(id = "assignment-1673")
    @CacheLookup
    private WebElement exammodeTotalMarks0;

    @FindBy(id = "hide")
    @CacheLookup
    private WebElement hide;

    @FindBy(css = "a[href='/courseware/courseslist/']")
    @CacheLookup
    private WebElement home;

    @FindBy(css = "a[href='/accounts/logout/']")
    @CacheLookup
    private WebElement logout;

    @FindBy(css = "a[href='#tab-1']")
    @CacheLookup
    private WebElement mainC;

    @FindBy(css = "a[href='/mission/']")
    @CacheLookup
    private WebElement mission;

    private final String pageLoadedText = "15 days 01:04:03 hours";

    private final String pageUrl = "/assignments/editor/1673";

    @FindBy(css = "button[title='Load Previous Submissions']")
    @CacheLookup
    private WebElement previousVersions;

    @FindBy(css = "#bs-navbar-collapse-1 ul.nav.navbar-nav.navbar-right li:nth-of-type(3) ul.dropdown-menu li:nth-of-type(1) a")
    @CacheLookup
    private WebElement profile;

    @FindBy(id = "wmd-input")
    @CacheLookup
    private WebElement remainingTime15Days010403Hours;

    @FindBy(css = "button[title='Click here to run on your input']")
    @CacheLookup
    private WebElement run;

    @FindBy(id = "dwnld-btn")
    @CacheLookup
    private WebElement savetobuffer;

    @FindBy(id = "select")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive1;

    @FindBy(id = "main.c_1")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive2;

    @FindBy(css = "#tab-1 div.CodeMirror.cm-s-3024-night.CodeMirror-empty div:nth-of-type(1) textarea")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive3;

    @FindBy(id = "compile")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive4;

    @FindBy(id = "choosesection")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive5;

    @FindBy(id = "execulable_file_name")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive6;

    @FindBy(id = "command_line_arguments")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive7;

    @FindBy(id = "customInput")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive8;

    @FindBy(id = "outputDisplay")
    @CacheLookup
    private WebElement shortcutsWhileCursorIsActive9;

    @FindBy(name = "customrun")
    @CacheLookup
    private WebElement submitInput;

    @FindBy(css = "button[title='Click here to submit and run on visible testcases']")
    @CacheLookup
    private WebElement submitrunpractice;

    @FindBy(css = "a[href='/team/']")
    @CacheLookup
    private WebElement team;

    @FindBy(css = "a.dropdown-toggle")
    @CacheLookup
    private WebElement testStudent;

    @FindBy(css = "button.navbar-toggle")
    @CacheLookup
    private WebElement toggleNavigation;

    public editorPage() {
    }

    public editorPage(WebDriver driver) {
        this();
        this.driver = driver;
        PageFactory.initElements( driver, this);
    }

    public editorPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
        PageFactory.initElements( driver, this);
    }

    public editorPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
        PageFactory.initElements( driver, this);
    }
    
}
