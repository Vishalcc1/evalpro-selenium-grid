package test;

//import java.io.File;
//import java.io.IOException;
//import java.net.InetAddress;
//import java.net.InetSocketAddress;
//import java.net.UnknownHostException;
//import java.time.Duration;
//import java.util.Queue;
//
//import org.littleshoot.proxy.ChainedProxy;
//import org.littleshoot.proxy.ChainedProxyManager;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Proxy;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import Pages.LoginPage;
//import io.netty.handler.codec.http.HttpRequest;
//import io.netty.util.internal.shaded.org.jctools.queues.MessagePassingQueue.Consumer;
//import net.lightbody.bmp.BrowserMobProxy;
//import net.lightbody.bmp.BrowserMobProxyServer;
//import net.lightbody.bmp.client.ClientUtil;
//import net.lightbody.bmp.core.har.Har;
//import net.lightbody.bmp.mitm.manager.ImpersonatingMitmManager;
//import net.lightbody.bmp.proxy.CaptureType;
//import net.lightbody.bmp.proxy.dns.AdvancedHostResolver;
//import net.lightbody.bmp.proxy.dns.HostResolver;
//
//public class GoogleTest {
//	
//	String driverPath = "F:/drivers/chromedriver/";
//	String sFileName = "/home/vishal/eclipse-workspace/demo2/abc.har";
//	
//	public WebDriver driver;
//	public BrowserMobProxy proxy;
//	
//	@BeforeTest
//	public void setUp() {
//		
//	   // start the proxy
//		BrowserMobProxyServer proxyServer = new BrowserMobProxyServer();
//		proxyServer.setTrustAllServers(true);
//		String ipAddress = "127.0.0.1"; // Replace with the desired IP address
//	    int port = 8082; // Replace with the port number of the proxy server
//	    InetSocketAddress ipsck = new InetSocketAddress(ipAddress, port);
//	   
//	    proxyServer.setChainedProxy(ipsck);
//	    proxyServer.setMitmManager(ImpersonatingMitmManager.builder().trustAllServers(true).build());
//
//		proxyServer.start(0);
//		 System.out.print(ipsck.getAddress()+" "+ proxyServer.getPort());
//        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
//        seleniumProxy.setHttpProxy("localhost:" + proxyServer.getPort());
//        seleniumProxy.setSslProxy("localhost:" + proxyServer.getPort());
//	    ChromeOptions options = new ChromeOptions();
//	    options.addArguments("--remote-allow-origins=*");
//	    options.setProxy(seleniumProxy);
//	    driver = new ChromeDriver(options);
//	    
////	    proxy.setChainedProxyManager(proxyManager);
////	    proxy.start();
////	    
////	    //get the Selenium proxy object - org.openqa.selenium.Proxy;
////	    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
////	    seleniumProxy.setHttpProxy(hostIp + ":" + proxy.getPort());
////        seleniumProxy.setSslProxy(hostIp + ":" + proxy.getPort());
////        
////	    // configure it as a desired capability
////	    DesiredCapabilities capabilities = new DesiredCapabilities();
////	    capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
////		
////	    //set chromedriver system property
//////		System.setProperty("webdriver.chrome.driver", "home/vishal/eclipse-workspace/demo2/chromedriver");
////		ChromeOptions options = new ChromeOptions();
////	    options.addArguments("--remote-allow-origins=*");
////	    options.merge(capabilities);
////	    driver = new ChromeDriver(options);
//////		driver = new ChromeDriver(capabilities);
////		
////	    // enable more detailed HAR capture, if desired (see CaptureType for the complete list)
////	    proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
////
////	    // create a new HAR with the label "seleniumeasy.com"
////	    proxy.newHar("seleniumeasy.com");
////
//	    // open seleniumeasy.com
//	    driver.get("https://cs101.bodhi.cse.iitb.ac.in/accounts/login/");
//	    
//	    Duration duration= Duration.ofSeconds(10);
//	    LoginPage objLogin= new LoginPage(driver);
//		objLogin.waitForPageLoad(duration);
//		objLogin.setByClickingSignUpTextField("student1");
//		objLogin.setByClickingSignUpPasswordField("root");
//		objLogin.signIn();
//		
//		driver.get("https://cs101.bodhi.cse.iitb.ac.in/assignments/details/1673");
//		
//	}
//	
//	@Test
//	public void testCaseOne() {
//		System.out.println("Navigate to selenium tutorials page");
//	}
//	
//	@AfterTest
//	public void tearDown() {
//
//		// get the HAR data
//		Har har = proxy.getHar();
//
//		// Write HAR Data in a File
//		File harFile = new File(sFileName);
//		try {
//			har.writeTo(harFile);
//		} catch (IOException ex) {
//			 System.out.println (ex.toString());
//		     System.out.println("Could not find file " + sFileName);
//		}
//		
//		if (driver != null) {
//			proxy.stop();
//			driver.quit();
//		}
//	}
//}

//import org.testng.annotations.Test;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;
//
//import java.net.InetSocketAddress;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.time.Duration;
//import java.time.LocalTime;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;
//import org.testng.annotations.Test;
//
//import Pages.LandingPage;
//
////	IMPORT THE PAGES
//
//import Pages.LoginPage;
//import Pages.assignmentListPage;
//import Pages.assignmentPage;
//import Pages.assignmentPageInstructor;
//import Pages.courseSidebarPage;
//import Pages.editorPage;
//import net.bytebuddy.asm.Advice.Local;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//
//import java.util.concurrent.TimeUnit;
//import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//
//import java.util.concurrent.TimeUnit;
//import org.openqa.selenium.By;
//import java.util.List;
//
//
//
//import java.io.IOException;
//import org.openqa.selenium.Proxy;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.BeforeSuite;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import net.lightbody.bmp.BrowserMobProxyServer;
//import net.lightbody.bmp.client.ClientUtil;
//import net.lightbody.bmp.proxy.CaptureType;
//
//public class GoogleTest {
//    WebDriver             driver;
//    BrowserMobProxyServer proxy;
//    Proxy                 seleniumProxy;
//
//    @Test(priority = 0)
//    public void startProxyServer() throws Exception {
//        proxy = new BrowserMobProxyServer();
//        proxy.start(8085);
//        proxy.newHar("h");
//    }
//    @Test(priority = 1)
//    public void configureProxy() throws Exception {
//    	String ipAddress=" 172.17.0.1";
//    	int port=8083;
//    	
//    }
//    @Test(priority = 2)
//    public void setUp() throws Exception {
//    	String hostIp=" 172.17.0.1";
//    	int port=8084;
//    	System.out.println(proxy.getPort());
//    	seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
//    	seleniumProxy.setHttpProxy(hostIp + ":" + proxy.getPort());
//        seleniumProxy.setSslProxy(hostIp + ":" + proxy.getPort());
//        proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
//
//    	DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--remote-allow-origins=*");
//        options.merge(capabilities);
//        driver = new ChromeDriver(options);
//        driver.get("google.com");
//    }
//
//    
//    @Test(priority = 3)
//    public void teardown() {
//        proxy.stop();
//        driver.quit();
//    }
//}

//package test;


import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import Pages.LandingPage;

//	IMPORT THE PAGES

import Pages.LoginPage;
import Pages.assignmentListPage;
import Pages.assignmentPage;
import Pages.assignmentPageInstructor;
import Pages.courseSidebarPage;
import Pages.editorPage;
import net.bytebuddy.asm.Advice.Local;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import java.util.List;



import java.io.IOException;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
////public class GoogleTest {
////   static String url ="https://whatismyipaddress.com/"; 	
////   
////   public static void main(String[] args) throws MalformedURLException {
////      // TODO Auto-generated method stub
////      
////      String ip = "8.208.85.34:8081";
////
////      Proxy proxy = new Proxy();
////      proxy.setHttpProxy(ip);
////      proxy.setSslProxy(ip);
////
////      ChromeOptions chromeOptions = new ChromeOptions();
////      chromeOptions.setCapability("proxy", proxy);
////
////      WebDriver driver = new ChromeDriver(chromeOptions);
//////	  driver = new RemoteWebDriver(new URL("http://172.17.0.1:4444"),cap);
////	  try {
////		driver.get(url);
////	  } catch (Exception e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////	  }
////	  driver.close();
////	  
////   }
////}
//
public class EditAssignment {
	
	LoginPage objLogin, objLogin1; // Login page objects
    LandingPage lpage, lpage1; // Landing page objects
    courseSidebarPage courseside, courseside1; // Course sidebar page objects
    assignmentListPage assgnlist, assgnlist1; // Assignment list page objects
    assignmentPageInstructor assign, assign1; // Instructor assignment page objects
    editorPage editpage1, editPage; // Editor page objects
    
	
	
	String url ="https://cs101.bodhi.cse.iitb.ac.in/accounts/login/"; 
	
	boolean parallelMode = false; // Flag to indicate distributed testing mode
	int waitTime=10;
	WebDriver driver,driver1;
	Duration duration = Duration.ofSeconds(waitTime);
	
	@BeforeTest
	public void DriverSetup() throws MalformedURLException, InterruptedException
	{ 
		//student 0 driver setup
//		DesiredCapabilities cap= new DesiredCapabilities();
//		cap.setBrowserName("chrome");
		System.setProperty("webdriver.chrome.driver","/home/vishal/Selenium/chromedriver");

		ChromeOptions browserDriverOptions = new ChromeOptions();
//		browserDriverOptions.setCapability("browserVersion", "111");
		driver = new RemoteWebDriver(new URL("http://172.17.0.1:4444"),browserDriverOptions);
		
//		driver = new RemoteWebDriver(new URL("http://172.17.0.1:4444"),cap);
		driver.get(url);
		
		//student 1 driver setup
		if(parallelMode) {
			DesiredCapabilities cap1= new DesiredCapabilities();
			cap1.setBrowserName("chrome");
			driver1 = new RemoteWebDriver(new URL("http://172.17.0.1:4444"),cap1);
			driver1.get(url);
		}
	}
	@Test(priority = 0)
	public void loginPage() throws InterruptedException
	{
		//student 0 login Page
		objLogin= new LoginPage(driver);
		objLogin.waitForPageLoad(duration);
		objLogin.setByClickingSignUpTextField("testinstructor");
		objLogin.setByClickingSignUpPasswordField("testinstructor");
		objLogin.signIn();
		
		//student 1 login Page
		if(parallelMode) {
			objLogin1= new LoginPage(driver1);
			objLogin1.waitForPageLoad(duration);
			objLogin1.setByClickingSignUpTextField("student1");
			objLogin1.setByClickingSignUpPasswordField("root");
			objLogin1.signIn();
		}
	}
	@Test(priority=1)
	public void landingPage() throws InterruptedException
	{
		//student 0 landing Page
		HashMap<String,String> data =new HashMap<String,String>();
		data.put("courseTitle","testingcode");
		data.put("courseCodeTitleText","CS2023: testingcode");
		
		lpage= new LandingPage(driver,data);
		lpage.waitForPageLoad(duration);
		lpage.openCourse();
		
		//student 1 landing Page
		if(parallelMode) {
			HashMap<String,String> data1 =new HashMap<String,String>();
			data1.put("courseTitle","testingcode");
			data1.put("courseCodeTitleText","CS2023: testingcode");
			
			lpage1 = new LandingPage(driver,data1);
			lpage1.waitForPageLoad(duration);
			lpage1.openCourse();
		}
	}	
	
	@Test(priority = 2)
	public void openProgram() throws InterruptedException
	{
		//student 0 Open Program
		courseside= new courseSidebarPage(driver);
		courseside.waitForPageLoadInstructor(duration);
		courseside.programLabsInstructor();
		
		//student 1 Open Program
		if(parallelMode) {
			courseside1= new courseSidebarPage(driver1);
			courseside1.waitForPageLoadInstructor(duration);
			courseside1.programLabsInstructor();
		}
	}	
	@Test(priority = 3)
	public void openAssignment() throws InterruptedException
	{
		//student 0 open assignment
		HashMap<String,String> data =new HashMap<String,String>();
		data.put("assignmentTitle","ExamMode");
		
		assgnlist= new assignmentListPage(driver,data);
		assgnlist.waitForPageLoad(duration);
		assgnlist.clickProgramAssignment();
		

		//student 1 open assignment
		if(parallelMode) {
			HashMap<String,String> data1 =new HashMap<String,String>();
			data1.put("assignmentTitle","ExamMode");
			
			assgnlist1= new assignmentListPage(driver1,data1);
			assgnlist1.waitForPageLoad(duration);
			assgnlist1.clickProgramAssignment();
		}
	}	
	@Test(priority = 4)
	public void openEditor() throws InterruptedException
	{
		//student 0 open editor
		assign= new assignmentPageInstructor(driver);
		assign.waitForPageLoad(duration);
		//add duration
//		TimeUnit.SECONDS.sleep(3);
		assign.clickEditAssignment();
		
		int year=assign.getCurrentYear(),month=assign.getCurrentMonth(),date=assign.getCurrentDate()
				,hour=assign.getCurrentHour(),minute=assign.getCurrentMinute(),
				timeslicehour=23,timesliceminute=5;
		
		assign.setDeadline(duration,year,month,date,hour,minute,timeslicehour,timesliceminute);
		TimeUnit.SECONDS.sleep(2);
		assign.clickSaveClose();
		
		
		//student 1 open editor
		if(parallelMode) {
//			assign1= new assignmentPage(driver1);
//			assign1.waitForPageLoad(duration);
//			assign1.clickEditorLink();
		}
		TimeUnit.SECONDS.sleep(4);
		
	}	
		
	
	@Test(priority = 5)
	public void DriverQui()
	{
		driver.quit();
		
		if(parallelMode) {
			driver1.quit();
		}
		
	}
	
	
}
